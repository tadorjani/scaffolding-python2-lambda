# -*- coding: utf-8 -*-

"""Module init."""

import logging
import logging.config
from .config import LOGGING_CONFIG

__version__ = '1.0.0'

logging.config.dictConfig(LOGGING_CONFIG)
LOGGER = logging.getLogger('scaffolding-python2-lambda')
LOGGER.info('Scaffolding Python2 Lambda')
