# -*- coding: utf-8 -*-

"""
src.config

Application configuration.
"""

CONFIG = {}

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        },
    },
    'handlers': {
        'stream_handler': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            'level': 'INFO',
        },
    },
    'root': {
        'level': 'DEBUG',
        'handlers': [
            'stream_handler',
        ],
    },
}
