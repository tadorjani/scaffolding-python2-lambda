# -*- coding: utf-8 -*-

"""
src.handler

AWS Lambda handler module.
"""

import logging

LOGGER = logging.getLogger(__name__)


def handler(event, context):
    """Lambda event handler entry point."""

    LOGGER.debug(event)
    LOGGER.debug(context)

    return
