#!/usr/bin/env bash

.PHONY clean clean-pyc clean-build

clean: clean-build clean-pyc clean-test

clean-build:
	rm -rf build/
	rm -rf dist/
	rm -rf .eggs/
	find . -name '*.egg-info' -type d -exec rm -rf {} +
	find . -name '*.eggs' -type d -exec rm -rf {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*.~' -exec rm -f {} +
	find . -name '__pycache__' -type d -exec rm -rf {} +

clean-test:
	rm -rf .tox/
	rm -rf .cache/

lint:
	flake8 src

test:
	pytest

dist:
	python2 setup.py check -r -s
	python2 setup.py ldist
	ls -lh dist
