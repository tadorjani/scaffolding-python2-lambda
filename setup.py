#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Build entry point.
"""

from setuptools import setup

setup(setup_requires=['pbr', 'lambda_setuptools'], pbr=True)
